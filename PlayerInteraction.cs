﻿using UnityEngine;
using System.Collections;

public class PlayerInteraction : MonoBehaviour 
{
	public float timeUntilNextGame = 4f;
	public float bounceForce = 10f;
	public GameObject explosion;


	public void DefeatEnemy()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, 0f);
		GetComponent<Rigidbody2D>().AddForce (new Vector2 (0f, bounceForce), ForceMode2D.Impulse);
	}

	public void Die()
	{
		Instantiate (explosion, transform.position, Quaternion.identity);
		gameObject.SetActive (false);
		Invoke ("ReloadLevel", timeUntilNextGame);
	}

	public void Win()
	{
		if(GetComponent<PlayerMotionStart> () != null)
			GetComponent<PlayerMotionStart> ().enabled = false;
		else
			GetComponent<PlayerMotionFinal> ().enabled = false;


		if(GetComponent<Animator> () != null)
			GetComponent<Animator> ().enabled = false;

		GetComponent<Rigidbody2D>().isKinematic = true;

		Invoke ("ReloadLevel", timeUntilNextGame);
	}

	void ReloadLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
}
