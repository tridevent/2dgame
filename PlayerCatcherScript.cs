﻿using UnityEngine;
using System.Collections;

public class PlayerCatcherScript : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag != "Player") return;
		
		other.gameObject.GetComponent<PlayerInteraction> ().Die ();
	}
}
