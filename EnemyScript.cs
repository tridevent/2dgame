﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour 
{
 	public Vector3 point1;
	public Vector3 point2;
	public float speedThrottle = .3f;
	public GameObject explosion;

	Vector3 target;
	Vector3 origin;
	float time;

	void Start()
	{
		PreparePath ();
	}

	void Update()
	{
		time += Time.deltaTime;
		transform.position = Vector3.Lerp (origin, target, time * speedThrottle);

		if ((transform.position - target).magnitude <= .01f)
			Flip();
	}

	void PreparePath()
	{
		if(transform.localScale.x > 0f)
		{
			transform.position = point1;
			target = point2;
		}
		else
		{
			transform.position = point2;
			target = point1;
		}

		origin = transform.position;
		time = 0f;
	}
	
	void Flip ()
	{
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;

		PreparePath ();
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.transform.tag != "Player") return;

		other.transform.GetComponent<PlayerInteraction> ().DefeatEnemy ();

		Instantiate (explosion, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag != "Player") return;

		other.gameObject.GetComponent<PlayerInteraction> ().Die ();
	}
}
