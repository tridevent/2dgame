﻿using UnityEngine;
using System.Collections;

public class ExplosionScript : MonoBehaviour 
{
	public float timeToLive = 1f;

	void Start()
	{
		Invoke ("Die", timeToLive);
	}

	void Die()
	{
		Destroy (gameObject);
	}
}
