﻿using UnityEngine;
using System.Collections;

public class PlayerMotionFinal: MonoBehaviour
{
	public bool jump = false;					
	public float jumpForce = 400f;			
	public float standardSpeed = 5f;
	public float mobileSpeed = 10f;
	
	bool grounded = false;
	bool facingRight = true;
	float move;

	Animator anim;


	void Awake()
	{
		anim = GetComponent<Animator>();
	}
	
	void OnCollisionEnter2D(Collision2D hit)
	{
		if(hit.gameObject.tag == "Ground")
			grounded = true;
	}
	
	void Update()
	{
		
#if UNITY_STANDALONE || UNITY_EDITOR
		move = Input.GetAxis("Horizontal");
		move *= standardSpeed;
#elif UNITY_IOS || UNITY_ANDROID
		move = Input.acceleration.x;
		move *= mobileSpeed;
#endif
		
		if((facingRight && move < 0) ||
		   (!facingRight && move > 0))
			Flip ();
		
		if((Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)) && grounded == true)
		{		
			jump = true;
			grounded = false;
			anim.SetTrigger("Jump");
		}

		anim.SetFloat ("Speed", Mathf.Abs (move));
	}
	
	void FixedUpdate ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2( move , GetComponent<Rigidbody2D>().velocity.y  );
		
		if(jump == true)
		{
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
			
			jump = false;
		}

		anim.SetBool("Grounded", grounded);
	}
	
	void Flip ()
	{
		facingRight = !facingRight;
		
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
